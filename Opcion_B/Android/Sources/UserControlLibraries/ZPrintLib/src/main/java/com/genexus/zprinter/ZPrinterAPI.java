package com.genexus.zprinter;

import com.artech.actions.ApiAction;
import com.artech.externalapi.ExternalApi;
import com.artech.externalapi.ExternalApiResult;

public class ZPrinterAPI extends ExternalApi {
    public static final String OBJECT_NAME = "ZPrinter";

    private static final String METHOD_PRINT = "Print";

    private static ZPrinter sPrinter;

    public ZPrinterAPI(ApiAction action) {
        super(action);
        addMethodHandler(METHOD_PRINT, 2, mPrintMethod);
    }

    private final IMethodInvoker mPrintMethod = parameters -> {
        String macAddress = (String)parameters.get(0);
        String printData = (String)parameters.get(1);

        sPrinter = new ZPrinter(this);

        sPrinter.print(macAddress, printData);
        return ExternalApiResult.SUCCESS_CONTINUE;
    };

}
