package com.genexus.zprinter;

import android.content.Context;

import com.artech.externalapi.ExternalApiDefinition;
import com.artech.externalapi.ExternalApiFactory;
import com.artech.framework.GenexusModule;

public class ZPrinterModule implements GenexusModule {

    @Override
    public void initialize(Context context) {
        ExternalApiFactory.addApi(new ExternalApiDefinition(ZPrinterAPI.OBJECT_NAME, ZPrinterAPI.class));
    }
}
