package com.genexus.zprinter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import com.artech.base.services.Services;

import org.apache.commons.io.FilenameUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import android.widget.Toast;
import android.util.Log;

import java.nio.charset.Charset;

import com.artech.externalapi.ExternalApi;

import com.zebra.android.comm.BluetoothPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnection;

import java.util.List;
import java.nio.charset.*;


// This code is based on https://www.androidcode.ninja/android-bluetooth-tutorial/
public class ZPrinter {
    
    ExternalApi gxApp ;
    public ZPrinter(ExternalApi app) {
        gxApp = app;
    }

    static final Charset utf8charset = Charset.forName("UTF-8");

    String toastMessage = "";

    public void print(String macAddress, String printData) {

        //Log.d("ZPrinter","Desde el objeto GX");

        // Here, thisActivity is the current activity

        String msg = "";

        try {
            ZebraPrinterConnection thePrinterConn = new BluetoothPrinterConnection(macAddress);
            thePrinterConn.open();
            String zplData = printData;
            thePrinterConn.write(zplData.getBytes(utf8charset));
            thePrinterConn.close();
            msg = "Impresión enviada !";
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
        }

        toastMessage = msg;
        gxApp.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(gxApp.getContext(), toastMessage, Toast.LENGTH_LONG).show();
            }
        });
    }
}
